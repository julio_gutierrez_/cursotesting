package ch.swissbytes.todo;

import spark.utils.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class HelperMethods {
    private static int PORT = 4567;

    public static UrlResponse doMethod(String requestMethod, String path, String body) {
        try {
            return getResponse(requestMethod, path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static UrlResponse getResponse(String requestMethod, String path) throws IOException {
        URL url = new URL("http://localhost:" + PORT + path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(requestMethod);
        connection.connect();
        String res = IOUtils.toString(connection.getInputStream());
        UrlResponse response = new UrlResponse();
        response.body = res;
        response.status = connection.getResponseCode();
        response.headers = connection.getHeaderFields();

        return response;
    }

    public static class UrlResponse {
        public Map<String, List<String>> headers;
        public String body;
        public int status;
    }
}
