<h1>My ToDo List</h1>
<ul>
    <#list todoitems as item>
        <li>
            ${item.id} - ${item.title}
        </li>
    </#list>
</ul>